<html>
    <head>
    	<?php include 'head.html'; ?>
        <title>halium port</title>
    </head>
    <body>
	<div id="container">
		<div id="header">
			<?php
			include 'header.html';
			?>
		</div>
		<div id="content">
			<?php
			include 'nav.html';
			?>
			<div id="main">
			<?php
			include 'halium/about.html';
			?>
			</div>
		</div>
		<div id="footer">
			Copyright &copy; 2020 James Franklin.
		</div>
    </body>
</html>
