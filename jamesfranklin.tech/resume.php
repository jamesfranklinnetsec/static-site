<html>
    <head>
    <?php
    include 'head.html';
    ?>
    </head>
    <body>
	<div id="container">
		<div id="header">
		        <h1><a href="index.html">jamesfranklin.tech</a></h1>
		</div>
	
		<div id="content">

			<?php
			include 'nav.html';
			?>
			
			<div id="main">
			<?php
			include 'about.html';
			include 'filler.html';
			?>
			</div>
			
		</div>
		
		<div id="footer">
			Copyright &copy; 2020 James Franklin.
		</div>
    </body>
</html>
