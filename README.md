# static-site

my static websites that I host

## jamesfranklin.tech
My personal website

## Usage
copy into your http servers root directory (usually /var/www/) and enable it.

## Contributing
I'm looking for security vulnerabilities where they are, please email me any issues and make changes as you please to improve my site :).

## License
[MIT](https://choosealicense.com/licenses/mit/)

